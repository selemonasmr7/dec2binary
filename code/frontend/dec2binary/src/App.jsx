import React, { useState } from 'react';
import axios from 'axios';
import './App.css'
function DecimalToBinaryConverter() {
    const [decimal, setDecimal] = useState('');
    const [binary, setBinary] = useState('');

    const handleConvert = async () => {
        try {
            const response = await axios.get(`http://localhost:8000/convert/?decimal=${decimal}`);
            setBinary(response.data.binary);
        } catch (error) {
            console.error('Error converting decimal to binary', error);
        }
    };
    return (
        <div>
            <p>
                <b>Decimal to Binary converter</b>
            </p>
            <input
                type="number"
                value={decimal}
                onChange={(e) => setDecimal(e.target.value)}
                placeholder="Enter a decimal number"
            />
            <button onClick={handleConvert}>Convert</button>
            {binary && <p>Binary: {binary}</p>}
        </div>
    );
}

export default DecimalToBinaryConverter;
