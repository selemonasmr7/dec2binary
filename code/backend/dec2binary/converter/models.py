from django.db import models


class Conversion(models.Model):
    decimal_value = models.IntegerField()
    binary_value = models.CharField(max_length=64)
