from django.http import JsonResponse, HttpResponseBadRequest
from .models import Conversion


def convert_decimal(request):
    if 'decimal' in request.GET:
        decimal_value = int(request.GET['decimal'])
        binary_value = bin(decimal_value)[2:]
        Conversion.objects.create(decimal_value=decimal_value, binary_value=binary_value)
        return JsonResponse({'binary': binary_value})
    else:
        return HttpResponseBadRequest("Decimal value is required: to do so go to the front end and fill the form")